
<!-- README.md is generated from README.Rmd. Please edit that file -->

# squirrelsarthur

<!-- badges: start -->
<!-- badges: end -->

The goal of squirrelsarthur is to provide squirrels tools.

## Installation

You can install the development version of squirrelsarthur like so:

``` r
# FILL THIS IN! HOW CAN PEOPLE INSTALL YOUR DEV PACKAGE?
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(squirrelsarthur)
## basic example code
```

Ajouter du texte plus tard pour presenter mon package a mes futurs
utilisateurs

``` r
get_message_fur_color("bleu")
#> We will focus on bleu squirrels
```
