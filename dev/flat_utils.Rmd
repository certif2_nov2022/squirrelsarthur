---
title: "flat_utils.Rmd empty"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r development, include=FALSE}
library(testthat)
```

```{r development-load}
# Load already included functions if relevant
pkgload::load_all(export_all = FALSE)
```

# launch_help
    
```{r function-launch_help}
#' launch_help
#' 
#' Open the index.html file
#' 
#' @importFrom utils browseURL
#'
#' @return side effect. Open a file
#' @export
#'
#' @examples
launch_help <- function(){
  browseURL(system.file("site", "index.html", package = "squirrelsarthur"))
}
```
  
```{r example-launch_help}
launch_help()
```
  
```{r tests-launch_help}
test_that("launch_help works", {
  expect_true(inherits(launch_help, "function")) 
  
  expect_error(
    object = launch_help(),
    regexp = NA
  )
})
```
  


```{r development-inflate, eval=FALSE}
# Run but keep eval=FALSE to avoid infinite loop
# Execute in the console directly
fusen::inflate(
  flat_file = "dev/flat_utils.Rmd", 
  vignette_name = "Some utils functions"
)
```

